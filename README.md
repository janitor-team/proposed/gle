
GL Extrusion Library
--------------------

This is an update to the very old GLE lirary, still available
[here](https://www.linas.org/gle/)

This update includes work from Dave Richards, adding new subsystems
for Windows users.
